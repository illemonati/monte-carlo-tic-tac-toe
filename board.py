from copy import deepcopy
from enum import Enum
import numpy as np


class BoardStatus(Enum):
    IN_PROGRESS = -1
    PLAYER0 = 0
    PLAYER1 = 1
    DRAW = 3


class Position:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return f'(x: {self.x}, y: {self.y})'

    def __str__(self):
        return self.__repr__()


class Board:
    def __init__(self):
        self.total_moves = 0
        self.board_state = np.full((3, 3), -1)

    def move(self, player_number: int, position: Position):
        # print(self.board_state)
        # print(self)
        self.total_moves += 1
        self.board_state[position.x][position.y] = player_number

    def get_available_positions(self):
        return [Position(i[0], i[1]) for i in np.argwhere(self.board_state == -1)]

    def is_over(self):
        return self.get_status() != BoardStatus.IN_PROGRESS

    def get_status(self):
        for i in range(0, 3):
            if self.board_state[i][0] == self.board_state[i][1] == self.board_state[i][2] and self.board_state[i][0] != -1:
                return BoardStatus(self.board_state[i][0])
            if self.board_state[0][i] == self.board_state[1][i] == self.board_state[2][i] and self.board_state[0][i] != -1:
                return BoardStatus(self.board_state[0][i])
        if self.board_state[0][0] == self.board_state[1][1] == self.board_state[2][2] and self.board_state[0][0] != -1:
            return  BoardStatus(self.board_state[0][0])
        if self.board_state[0][2] == self.board_state[1][1] == self.board_state[2][0] and self.board_state[0][2] != -1:
            return  BoardStatus(self.board_state[0][2])
        if len(self.get_available_positions()) > 0:
            return BoardStatus.IN_PROGRESS
        return BoardStatus.DRAW

    def __deepcopy__(self, memo):
        res = self.__class__()
        res.board_state = np.array(self.board_state)
        res.total_moves = self.total_moves
        return res
