from copy import deepcopy

from board import Board, Position
from mcts import MonteCarloTreeSearch
import numpy as np
from structures import Node


def test_ai():
    player_turn = 0
    board = Board()
    while not board.is_over():
        search = deepcopy(MonteCarloTreeSearch())
        print(board.board_state)
        board = search.find_next_move(board, player_turn, 1000)
        player_turn = 1 - player_turn
    print("ues")
    print(board.board_state)



def human_test(player=0):
    player_turn = 0
    board = Board()
    while not board.is_over():
        print(board.board_state)
        if player_turn == player:
            movex = int(input('x: '))
            movey = int(input('y: '))
            board.move(player_turn, Position(movex, movey))
        else:
            search = deepcopy(MonteCarloTreeSearch())
            board = search.find_next_move(board, player_turn, 1000)
        player_turn = 1 - player_turn
    print("ues")
    print(board.board_state)


if __name__ == '__main__':
    test_ai()
    human_test()