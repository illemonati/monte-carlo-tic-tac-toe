from copy import deepcopy

from board import Board, Position, BoardStatus
import numpy as np

from mcts import MonteCarloTreeSearch


class Connect4Board(Board):
    def __init__(self):
        super().__init__()
        self.board_state = np.full((7, 6), -1)

    def get_available_positions(self):
        positions = []
        for (x, col) in enumerate(self.board_state):
            space = np.argwhere(col == -1)
            if len(space) > 0:
                positions.append(Position(x, space[0][0]))
        return positions

    def square_win(self, x, y, show_res=False):
        self.board_state = self.board_state
        if self.board_state[x][y] == -1:
            return False

        if x-1 > 0 and x-2 >0 and x-3 > 0:
            try:
                if self.board_state[x][y] == self.board_state[x-1][y+1] == self.board_state[x-2][y+2] == self.board_state[x-3][y+3]:
                    return True
            except IndexError:
                pass
            if y-1 > 0 and y-2 > 0 and y-3 > 0:
                if self.board_state[x][y] == self.board_state[x-1][y-1] == self.board_state[x-2][y-2] == self.board_state[x-3][y-3]:
                    return True
        try:
            if self.board_state[x][y] == self.board_state[x][y+1] == self.board_state[x][y+2] == self.board_state[x][y+3]:
                return True
        except IndexError:
            pass
        try:
            if self.board_state[x][y] == self.board_state[x+1][y] == self.board_state[x+2][y] == self.board_state[x+3][y]:
                return True
        except IndexError:
            pass
        return False


    def get_status(self, show_res=False):
        if len(self.get_available_positions()) == 0:
            return BoardStatus.DRAW
        for x in range(len(self.board_state)):
            for y in range(len(self.board_state[0])):
                if self.square_win(x, y, show_res):
                    if show_res:
                        print(x, y)
                    return BoardStatus(self.board_state[x] [y])
        return BoardStatus.IN_PROGRESS


    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return str(np.rot90(self.board_state)).replace('0', 'B').replace('-1', '  ').replace('1', 'R')


def test_ai():
    player_turn = 0
    board = Connect4Board()
    print(board)
    while not board.is_over():
        search = MonteCarloTreeSearch(win_score=100)
        print(board)
        board = search.find_next_move(board, player_turn, 1000)
        player_turn = 1 - player_turn
    print(board.board_state)
    print(board.get_status(True))
    print(board)


def human_test(player=0):
    player_turn = 1
    board = Connect4Board()
    while not board.is_over():
        print(board)
        if player_turn == player:
            movex = int(input('x: '))
            moves = board.get_available_positions()
            move = [move for move in moves if move.x == movex][0]
            board.move(player_turn, move)
        else:
            search = deepcopy(MonteCarloTreeSearch())
            board = search.find_next_move(board, player_turn, 1000)
        player_turn = 1 - player_turn
    print(board.board_state)

if __name__ == '__main__':
    test_ai()
    # human_test()




