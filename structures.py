from __future__ import annotations

from copy import deepcopy
from pprint import pprint
from typing import List
import random

import board
import math
import sys
from typing import TYPE_CHECKING


sqrt2 = math.sqrt(2)

Board = board.Board


class State:
    def __init__(self, board: Board = None, player_number: int = 0, visit_count: int = 0, win_score: float = 10):
        self.board = board
        self.player_number = player_number
        self.visit_count = visit_count
        self.win_score = win_score

    def get_opponent(self):
        return 1 - self.player_number

    def get_all_possible_states(self):
        possible_states = []
        available_positions = self.board.get_available_positions()
        for position in available_positions:
            new_state = State(deepcopy(self.board))
            new_state.player_number = self.get_opponent()
            new_state.board.move(new_state.player_number, position)
            possible_states.append(new_state)
        return possible_states

    def toggle_player(self):
        self.player_number = self.get_opponent()

    def random_play(self):
        available_positions = self.board.get_available_positions()
        position = random.choice(available_positions)
        self.board.move(self.player_number, position)

    def __deepcopy__(self, memo):
        res = State()
        res.board = deepcopy(self.board)
        res.player_number = self.player_number
        res.visit_count = self.visit_count
        res.win_score = self.win_score
        return res


class Node:
    def __init__(self, state: State = State(), parent: Node = None, child_array: List[Node] = None):
        if child_array is None:
            child_array = []
        self.state = state
        self.parent = parent
        self.child_array = child_array

    def select_promising_node(self) -> Node:
        node = self
        while len(node.child_array) > 0:
            node = UCT.find_best_node_with_uct(node)
        return node

    def expand_node(self):
        possible_states = self.state.get_all_possible_states()
        for state in possible_states:
            new_node = Node(state=deepcopy(state))
            new_node.parent = self
            new_node.state.player_number = self.state.get_opponent()
            self.child_array.append(new_node)

    def get_random_child_node(self) -> Node:
        return random.choice(self.child_array)

    def get_child_with_max_score(self):
        if len(self.child_array) == 0:
            return None
        return max(self.child_array, key=lambda child: child.state.visit_count)

    def __deepcopy__(self, memo):
        res = Node()
        res.child_array = []
        for child in self.child_array:
            res.child_array.append(deepcopy(child))
        res.state = deepcopy(self.state)
        res.parent = self.parent
        return res


class UCT:
    @staticmethod
    def get_uct_val(total_visit: int, node_win_score: float, node_visit: int):
        if node_visit == 0:
            return sys.maxsize
        return (node_win_score / node_visit) + sqrt2 * math.sqrt(math.log10(total_visit) / node_visit)

    @staticmethod
    def find_best_node_with_uct(root_node: Node):
        root_visit_count = root_node.state.visit_count
        return max(root_node.child_array,
                   key=lambda node: UCT.get_uct_val(root_visit_count, node.state.win_score, node.state.visit_count))



class Tree:
    def __init__(self, root: Node = Node()):
        self.root = root
