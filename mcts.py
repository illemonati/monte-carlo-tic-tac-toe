from copy import deepcopy
from pprint import pprint
from board import BoardStatus
from structures import Tree, Node
import time
import sys


class MonteCarloTreeSearch:
    def __init__(self, level: int = 3, opponent: int = 1, win_score: int = 10, player: int = 0):
        self.win_score = win_score
        self.level = level
        self.opponent = opponent
        self.player = player

    def find_next_move(self, board, player_number: int, time_limit_ms: int):
        self.player = player_number
        self.opponent = 1 - player_number
        tree = deepcopy(Tree())
        tree.root.state.board = board
        tree.root.state.player_number = self.opponent
        end_time = time.time_ns() + (time_limit_ms * 1e6)
        while time.time_ns() < end_time:
            promising_node = tree.root.select_promising_node()
            if promising_node.state.board.get_status() == BoardStatus.IN_PROGRESS:
                promising_node.expand_node()
            node_to_explore = promising_node
            if len(promising_node.child_array) > 0:
                node_to_explore = promising_node.get_random_child_node()
            playout_result = self.simulate_random_playout(node_to_explore)
            self.back_propogation(node_to_explore, playout_result)
        winner_node = tree.root.get_child_with_max_score()
        return winner_node.state.board

    def back_propogation(self, node: Node, player_number: int):
        temp = node
        while temp is not None:
            temp.state.visit_count += 1
            if temp.state.player_number == player_number:
                temp.state.win_score += self.win_score
            temp = temp.parent

    def simulate_random_playout(self, node: Node):
        temp = deepcopy(node)
        # print(id(node) == id(temp))
        # print(temp.state.board.board_state)
        if temp.state.board.get_status().value == self.opponent:
            temp.parent.state.win_score = -1000000
            return temp.state.board.get_status().value
        if temp.state.board.get_status().value == self.player:
            temp.parent.state.win_score = self.win_score
            return temp.state.board.get_status().value
        while temp.state.board.get_status() == BoardStatus.IN_PROGRESS:
            # print(temp.state)
            temp.state.toggle_player()
            temp.state.random_play()
        return temp.state.board.get_status()